package com.games.chess.logic.pieces;

import java.util.HashSet;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;

public abstract class Piece {

	Board board;
	private Team team;
	boolean moved = false;

	//wenn "filterAgainstCheck" true ist, wird kein Zug m�glich ist, der zu einem Schach auf den eigenen K�nig f�hrt
	public static boolean filterAgainstCheck = true;

	
	//Konstruktor
	public Piece(Board board, Team team) {
		this.board = board;
		this.team = team;
	}


	//Alle m�glichen Positionen Holen, in die sich eine Figur bewegen kann
	public abstract HashSet<Position> getAllPossibleMoves(Position position);

	
	public abstract Piece clone(Board board);

	
	void add(HashSet<Position> hashSet, Position position, Position destination) {

		if (filterAgainstCheck && board.getTurn() == getTeam()) {
			if (!board.wouldBeCheck(position, destination)) {
				hashSet.add(destination);
			}
		} else {
			hashSet.add(destination);
		}
		
	}

	
//	die Methode "blocked" verhindert, dass eine Figur �ber eine andere 
//	Figur springt.(beim Springer ist es erlaubt)
	boolean blocked(HashSet<Position> hashSet, Position position, Position destination) {
		if (!isInBounds(destination)) {
			return true;
		} else if (isEmptySpot(destination)) {
			add(hashSet, position, destination);
			return false;
		} else if (isSameTeam(destination)) {
			return true;
		} else { // eine Figur des Gegners schlagen.
			add(hashSet, position, destination);
			return true;
		}
	}

	public Team getTeam() {
		return team;
	}
	
//	"isInBounds" �berpr�ft, ob eine bestimmte Position nicht au�erhalb 
//	der Grenzen des Schachbretts liegt.
	boolean isInBounds(Position position) {
		return Board.isInBounds(position);
	}

//	"isEmptySpot" �berpr�ft, ob eine bestimmte Position auf dem Brett leer ist.
	boolean isEmptySpot(Position position) {
		return board.isEmptySpot(position);
	}

//	"isSameTeam" �berpr�ft, ob diese Figur im selben Team ist wie eine Figur auf einer bestimmten Position.
	boolean isSameTeam(Position position) {
		return board.sameTeam(this, board.get(position));
	}

	
//	"isValidSpot" �berpr�ft, ob sich eine Figur in eine bestimmte Position bewegen kann.
	boolean isValidSpot(Piece piece, Position position) {
		return board.isValidSpot(piece, position);
	}

//	"safeSpot" �berpr�ft, ob eine bestimmte Position auf dem Brett zum aktuellen 
//	Zeitpunkt vor feindlichen Figuren sicher ist.
	boolean safeSpot(Team team, Position position) {
		return board.safeSpot(team, position);
	}

//	"hasMoved" �berpr�ft, ob die Figur bewegt wurde.
//	(N�tzlich f�r die Rochade)
	public boolean hasMoved() {
		return moved;
	}

//	Markiert die Figur als "wurde bewegt".
//	(N�tzlich f�r die Rochade)
	public void markAsMoved() {
		moved = true;
	}

	public static void filterAgainstCheckON() {
		filterAgainstCheck = true;
	}

	public static void filterAgainstCheckOFF() {
		filterAgainstCheck = false;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;
		if (o == null || getClass() != o.getClass())
			return false;

		Piece piece = (Piece) o;

		if (moved != piece.moved)
			return false;
		return getTeam() == piece.getTeam();
	}

	@Override
	public int hashCode() {
		int result = team.hashCode();
		result = 31 * result + (moved ? 1 : 0);
		result = 31 * result + getClass().hashCode();
		return result;
	}

	@Override
	public String toString() {
		String team = getTeam().toString();
		team = team.toLowerCase();
		team = team.substring(0, 1).toUpperCase() + team.substring(1);
		return team;
	}

}
