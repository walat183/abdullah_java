package com.games.chess.logic.pieces;

import java.util.HashSet;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;


public class Queen extends Piece {

//	konstruktor
	public Queen(Board board, Team team) {
		super(board, team);
	}

	@Override
	public HashSet<Position> getAllPossibleMoves(Position position) {

		HashSet<Position> hashSet = new HashSet<>();

		// Queen hat die gleichen Z�ge wie Bishop und Rook zusammen.
		hashSet.addAll(new Rook(board, getTeam()).getAllPossibleMoves(position));
		hashSet.addAll(new Bishop(board, getTeam()).getAllPossibleMoves(position));

		return hashSet;
	}

	
	public Queen clone(Board board) {
		Queen queen = new Queen(board, getTeam());
		queen.moved = this.moved;
		return queen;
	}

	@Override
	public String toString() {
		return "Queen";
	}

}
