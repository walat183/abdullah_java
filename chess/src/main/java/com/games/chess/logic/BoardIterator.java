package com.games.chess.logic;

import java.util.Iterator;
import java.util.NoSuchElementException;

import com.games.chess.logic.pieces.Piece;

class BoardIterator implements Iterator<Position> {

	private Piece[][] array;
	private Position current = new Position(0, 0);

	BoardIterator(Piece[][] array) {
		this.array = array;
		current = findNext(current);
	}

	private Position findNext(Position p) {

		while (p.getY() < 8 && array[p.getY()][p.getX()] == null) {
			p = increment(p);
		}

		return p;
	}

	public boolean hasNext() {
		return current.getX() < 8 && current.getY() < 8;
	}

	private Position increment(Position position) {

		if (position.getX() < 7) {
			position = new Position(position.getX() + 1, position.getY());
		} else {
			position = new Position(0, position.getY() + 1);
		}

		return position;
	}

	public Position next() {

		if (!hasNext()) {
			throw new NoSuchElementException();
		}

		Position prev = current;
		current = increment(current);
		current = findNext(current);

		return prev;
	}

}
