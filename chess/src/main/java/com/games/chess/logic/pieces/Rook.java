package com.games.chess.logic.pieces;

import java.util.HashSet;

import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;


public class Rook extends Piece {

//	konstruktor
	public Rook(Board board, Team team) {
		super(board, team);
	}

	@Override
	public HashSet<Position> getAllPossibleMoves(Position position) {
		HashSet<Position> hashSet = new HashSet<>();
		loadNorth(hashSet, position);
		loadSouth(hashSet, position);
		loadWest(hashSet, position);
		loadEast(hashSet, position);
		return hashSet;
	}

//	L�dt alle nordw�rts gerichteten Positionen, zu denen sich der Turm bewegen kann.
	private void loadNorth(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position p = new Position(position.getX(), position.getY() - i);
			if (blocked(hashSet, position, p)) {
				return;
			}
		}
	}

//	L�dt alle s�dw�rts gerichteten Positionen, zu denen sich der Turm bewegen kann.
	private void loadSouth(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position p = new Position(position.getX(), position.getY() + i);
			if (blocked(hashSet, position, p)) {
				return;
			}
		}
	}

//	L�dt alle westw�rts gerichteten Positionen, zu denen sich der Turm bewegen kann.
	private void loadWest(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position p = new Position(position.getX() - i, position.getY());
			if (blocked(hashSet, position, p)) {
				return;
			}
		}
	}

//	L�dt alle ostw�rts gerichteten Positionen, zu denen sich der Turm bewegen kann.
	private void loadEast(HashSet<Position> hashSet, Position position) {
		for (int i = 1; i < 8; i++) {
			Position p = new Position(position.getX() + i, position.getY());
			if (blocked(hashSet, position, p)) {
				return;
			}
		}
	}

	public Rook clone(Board board) {
		Rook rook = new Rook(board, getTeam());
		rook.moved = this.moved;
		return rook;
	}

	@Override
	public String toString() {
		return "Rook";
	}

}
