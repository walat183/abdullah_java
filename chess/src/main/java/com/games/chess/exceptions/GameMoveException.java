package com.games.chess.exceptions;

public class GameMoveException extends RuntimeException {


	private static final long serialVersionUID = 1L;

	public GameMoveException(String message) {
		super(message);
	}

}
