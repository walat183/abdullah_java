package com.games.chess.entities;

import java.util.HashSet;
import java.util.Set;

import com.games.chess.exceptions.GameMoveException;
import com.games.chess.logic.Board;
import com.games.chess.logic.Position;
import com.games.chess.logic.Team;
import com.games.chess.logic.pieces.Piece;
import com.games.chess.services.GameStateBuildingService;
import com.games.chess.services.RandomTeamAssigningService;

public class ChessGame {

	private Set<Player> players;
	private Board board;
	private RandomTeamAssigningService randomTeamAssigningService;
	private static final GameStateBuildingService gameStateBuildingService = new GameStateBuildingService();

	public ChessGame() {
		this.players = new HashSet<>();
		this.randomTeamAssigningService = new RandomTeamAssigningService();

		this.board = new Board();
	}

	public void registerPlayer(Player player) {
		Team randomTeam = randomTeamAssigningService.getRandomTeam();
		player.setTeam(randomTeam);
		players.add(player);
	}

	public int getCurrentPlayersSize() {
		return players.size();
	}

	public GameState getGameStateObject(String gameId, String playerId) {
		GameState gameState = gameStateBuildingService.buildGameState(playerId, gameId, board);
		return gameState;
	}

	public void makePlayerMove(String gameId, String playerId, int startX, int startY, int destinationX,
			int destinationY) {
		Position fromPosition = new Position(startX, startY);
		Piece pieceToBeMoved = board.get(fromPosition);
		Position toPosition = new Position(destinationX, destinationY);

		if (!board.isValidSpot(pieceToBeMoved, toPosition)) {
			throw new GameMoveException("invalid move: Eating same team Pieces / attemping to move outside the board");

		} else if (board.wouldBeCheck(fromPosition, toPosition)) {
			throw new GameMoveException("invalid move: king will be in check after doing this move");

		} else if (players.size() < 2) {
			throw new GameMoveException("Not Enough players in game");

		} else if (!playerShouldMove(playerId)) {
			throw new GameMoveException("Not your turn");
		} else {
			board.move(fromPosition, destinationX, destinationY);

		}

	}

	private boolean playerShouldMove(String playerId) {
		for (Player player : players) {
			if (player.getPlayerId().equals(playerId)) {
				return player.getTeam().equals(board.getTurn());
			}
		}
		return false;
	}

}
