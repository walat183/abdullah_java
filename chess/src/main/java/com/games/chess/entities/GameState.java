package com.games.chess.entities;

import java.util.List;

import com.games.chess.logic.Team;

public class GameState {

	private Team playerTurn;
	private boolean gameOver;
	private Team winner;
	private List<BoardUnit> blackPieces;
	private List<BoardUnit> whitePieces;

	public Team getPlayerTurn() {
		return playerTurn;
	}

	public void setPlayerTurn(Team playerTurn) {
		this.playerTurn = playerTurn;
	}

	public boolean isGameOver() {
		return gameOver;
	}

	public void setGameOver(boolean gameOver) {
		this.gameOver = gameOver;
	}

	public Team getWinner() {
		return winner;
	}

	public void setWinner(Team winner) {
		this.winner = winner;
	}

	public List<BoardUnit> getBlackPieces() {
		return blackPieces;
	}

	public void setBlackPieces(List<BoardUnit> blackPieces) {
		this.blackPieces = blackPieces;
	}

	public List<BoardUnit> getWhitePieces() {
		return whitePieces;
	}

	public void setWhitePieces(List<BoardUnit> whitePieces) {
		this.whitePieces = whitePieces;
	}

}
