package com.games.chess.controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.games.chess.entities.GameState;
import com.games.chess.exceptions.GameMoveException;
import com.games.chess.manager.ChessGamesManager;

@RestController
public class ChessController {

	private ChessGamesManager chessGameManager;

	public ChessController(ChessGamesManager chessGameManager) {
		this.chessGameManager = chessGameManager;
	}

	@RequestMapping("/games")
	public @ResponseBody ResponseEntity<String> getNewGameId() {
		String gameId = chessGameManager.registerNewGame();
		return ResponseEntity.ok(gameId);
	}

	@PostMapping("/players/{gameId}")
	public @ResponseBody ResponseEntity<String> getNewGameId(@PathVariable(name = "gameId") String gameId) {
		String playerId = chessGameManager.registerNewPlayer(gameId);
		return ResponseEntity.ok(playerId);
	}

	@GetMapping(value = "/games/{gameId}/states/{playerId}", produces = "application/json")
	public @ResponseBody ResponseEntity<GameState> getNewGameId(@PathVariable(name = "gameId") String gameId,
			@PathVariable(name = "playerId") String playerId) {
		GameState gameState = chessGameManager.getGameState(gameId, playerId);
		return ResponseEntity.ok(gameState);
	}

	@PostMapping(value = "/games/{gameId}/moves", consumes = "application/json", produces = "application/json")
	public @ResponseBody ResponseEntity<MoveResposne> postMove(@PathVariable(name = "gameId") String gameId,
			@RequestBody MoveRequest moveRequest) {
		chessGameManager.makePlayerMove(gameId, moveRequest.getPlayerId(), moveRequest.getStartX(),
				moveRequest.getStartY(), moveRequest.getDestinationX(), moveRequest.getDestinationY());
		return ResponseEntity.ok(new MoveResposne(true, "OK"));
	}

	@ExceptionHandler({ GameMoveException.class })
	public ResponseEntity<MoveResposne> handleMoveException(GameMoveException e) {
		return ResponseEntity.ok(new MoveResposne(false, e.getMessage()));
	}

}
