package com.games.chess.controller;

public class MoveResposne {

	private boolean isAccepted;
	private String message;

	public MoveResposne(boolean isAccepted, String message) {
		this.isAccepted = isAccepted;
		this.message = message;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
