package com.games.chess.services;

import java.util.Random;

import com.games.chess.logic.Team;

public class RandomTeamAssigningService {

	private boolean randomTeamAssigned;
	private Team remainingTeam;

	public Team getRandomTeam() {
		int random = new Random().nextInt(2);

		if (randomTeamAssigned) {
			return remainingTeam;
		}
		randomTeamAssigned = true;
		if (random == 0) {
			remainingTeam = Team.WHITE;
			return Team.BLACK;
		} else {
			remainingTeam = Team.BLACK;
			return Team.WHITE;
		}
	}

}
