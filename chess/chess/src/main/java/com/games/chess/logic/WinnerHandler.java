package com.games.chess.logic;

import java.util.HashMap;
import java.util.Stack;

import com.games.chess.logic.peaces.Piece;

class WinnerHandler {

	private Board board;

	WinnerHandler(Board board) {
		this.board = board;
	}

	boolean wouldBeCheck(Position position, Position destination) {

		Piece temp = null;
		Piece piece = board.get(position);
		boolean result;

		if (!board.isEmptySpot(destination)) {
			temp = board.get(destination).clone(board);
		}

		move(piece, position, destination);
		Team team = piece.getTeam();
		result = !board.safeSpot(team, board.getKing(team));
		move(piece, destination, position);

		if (temp != null) {
			board.add(temp, destination);
		}

		return result;
	}

	private boolean wouldBeCheckmate(Position position, Position destination) {
		Piece temp = null;
		Piece piece = board.get(position);
		boolean result;

		if (!board.isEmptySpot(destination)) {
			temp = board.get(destination).clone(board);
		}

		move(piece, position, destination);
		result = piece.getAllPossibleMoves(destination).size() == 0;
		move(piece, destination, position);

		if (temp != null) {
			board.add(temp, destination);
		}

		return result;
	}

	private void move(Piece piece, Position position, Position destination) {
		board.add(piece, destination);
		board.delete(position);
	}

	private boolean isStalemate(Team team) {
		for (Position position : board) {
			if (board.get(position).getTeam() != team) {
				continue;
			}
			for (Position pos : board.get(position).getAllPossibleMoves(position)) {
				if (!wouldBeCheckmate(position, pos)) {
					return false;
				}
			}
		}
		return true;
	}

	private boolean isCheckmate(Team team) {
		for (Position position : board) {
			if (board.get(position).getTeam().equals(team)) {
				if (board.get(position).getAllPossibleMoves(position).size() != 0) {
					return false;
				}
			}
		}

		return true;
	}

	private boolean isDraw() {
		boolean onePieceLeft = onePieceLeft();
		boolean tripleTurn = tripleMove(Team.WHITE) && tripleMove(Team.BLACK);
		return onePieceLeft || tripleTurn;
	}

	private boolean onePieceLeft() {
		Stack<Position> whitePieces = new Stack<>();
		for (Position p : board) {
			if (board.get(p).getTeam().equals(Team.WHITE)) {
				whitePieces.push(p);
			}
		}

		Stack<Position> blackPieces = new Stack<>();
		for (Position p : board) {
			if (board.get(p).getTeam().equals(Team.BLACK)) {
				blackPieces.push(p);
			}
		}

		return blackPieces.size() == 1 && whitePieces.size() == 1;
	}

	private boolean tripleMove(Team team) {
		HashMap<Move, Integer> hashMap = new HashMap<>();
		for (Move m : board.movementHandler.moves) {
			if (m.getPiece1().getTeam() != team)
				continue;
			if (!hashMap.containsKey(m)) {
				hashMap.put(m, 1);
			} else {
				int num = hashMap.get(m);
				hashMap.put(m, ++num);
			}
			if (hashMap.get(m) == 3) {
				return true;
			}
		}
		return false;
	}

	boolean checkIfGameIsOver() {

		if (isCheckmate(Team.WHITE)) {
			board.setWinner(Team.BLACK);
			return true;
		} else if (isCheckmate(Team.BLACK)) {
			board.setWinner(Team.WHITE);
			return true;
		} else if (isStalemate(Team.WHITE)) {
			board.setWinner(Team.BLACK);
			return true;
		} else if (isStalemate(Team.BLACK)) {
			board.setWinner(Team.WHITE);
			return true;
		} else if (isDraw()) {
			board.setWinner(null);
			return true;
		}

		return false;
	}

	void declareWinner() {
		if (!board.isGameOver()) {
			throw new IllegalStateException("Cannot declare winner if game is not over.");
		}

		String msg;

		if (board.getWinner() == null) {
			msg = "The game is a draw.";
		} else if (board.getWinner() == Team.WHITE) {
			msg = "White wins!";
		} else {
			msg = "Black wins!";
		}

		MessageBox msgBox = new MessageBox(msg);
		Thread th = new Thread(msgBox);
		th.start();
	}

	private class MessageBox implements Runnable {

		private String message;

		MessageBox(String message) {
			this.message = message;
		}

		@Override
		public void run() {
			// JOptionPane.showMessageDialog(null, message);
		}
	}

}
