package com.games.chess.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.games.chess.entities.GamesRegister;
import com.games.chess.generator.GameIdGenerator;
import com.games.chess.generator.IdGenerator;
import com.games.chess.generator.PlayerIdGenerator;
import com.games.chess.manager.BasicGameManager;
import com.games.chess.manager.ChessGamesManager;

@Configuration
public class BeansConfigurations {

	@Bean
	IdGenerator gameIdGenerator() {
		return new GameIdGenerator();
	}

	@Bean
	IdGenerator playerIdGenerator() {
		return new PlayerIdGenerator();
	}

	@Bean
	ChessGamesManager basicGameManager(IdGenerator playerIdGenerator, IdGenerator gameIdGenerator,
			GamesRegister gamesRegister) {
		return new BasicGameManager(playerIdGenerator, gameIdGenerator, gamesRegister);
	}

	@Bean
	GamesRegister gamesRegister() {
		return new GamesRegister();
	}

}
