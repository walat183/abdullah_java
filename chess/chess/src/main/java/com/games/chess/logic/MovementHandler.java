package com.games.chess.logic;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Stack;

import com.games.chess.exceptions.GameMoveException;
import com.games.chess.logic.peaces.King;
import com.games.chess.logic.peaces.Pawn;
import com.games.chess.logic.peaces.Piece;
import com.games.chess.logic.peaces.Queen;
import com.games.chess.logic.peaces.Rook;

public class MovementHandler implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private Board board;
	private Position enPassant;
	Stack<Move> moves;

	MovementHandler(Board board) {
		this.board = board;
		enPassant = null;
		moves = new Stack<>();
	}

	void move(Position position, int x, int y) {

		HashSet<Position> hashSet = board.get(position).getAllPossibleMoves(position);
		Position destination = new Position(x, y);

		if (!hashSet.contains(destination)) {
			throw new GameMoveException("Piece can't move to this location");
		}

		Move move = new Move(board, position, destination);
		move.setEnPassant(enPassant);
		moves.push(move);

		markEnPassantPosition(position, destination);
		performEnPassant(position, destination);
		castling(position, destination);
		markAsMoved(position);
		board.move(position, destination);

		pawnPromotion(destination);
	}

	boolean undo() {
		if (moves.isEmpty()) {
			return false;
		}

		Move move = moves.pop();

		Piece p1 = move.getPiece1();
		Piece p2 = move.getPiece2();
		Piece p3 = move.getPiece3();
		Piece p4 = move.getPiece4();

		Position pos1 = move.getPosition1();
		Position pos2 = move.getPosition2();
		Position pos3 = move.getPosition3();
		Position pos4 = move.getPosition4();

		Position enPassant = move.getEnPassant();
		setEnPassant(enPassant);

		board.add(p1, pos1);
		board.updateKings(p1, pos1);

		if (p2 != null) {
			board.add(p2, pos2);
			board.updateKings(p2, pos2);
		} else {
			board.add(null, pos2);
		}

		if (p3 != null) {
			board.add(p3, pos3);
			board.updateKings(p3, pos3);
		}

		if (p4 != null) {
			board.add(p4, pos4);
			board.updateKings(p4, pos4);
		} else {
			if (pos4 != null) {
				board.add(null, pos4);
			}
		}

		return true;
	}

	int getMoveCount() {
		return moves.size();
	}

	private Position performEnPassant(Position position, Position destination) {

		if (enPassantOccurredGoingNorth(position, destination)) {
			Position pos = new Position(destination.getX(), destination.getY() + 1);
			moves.peek().setPosition3(pos);
			board.delete(pos);
			return pos;
		}

		if (enPassantOccurredGoingSouth(position, destination)) {
			Position pos = new Position(destination.getX(), destination.getY() - 1);
			moves.peek().setPosition3(pos);
			board.delete(pos);
			return pos;
		}

		return null;
	}

	private boolean enPassantOccurredGoingNorth(Position position, Position destination) {
		return pawnIsGoingNorth(position) && destinationIsDiagonal(position, destination)
				&& board.isEmptySpot(destination);
	}

	private boolean enPassantOccurredGoingSouth(Position position, Position destination) {
		return pawnIsGoingSouth(position) && destinationIsDiagonal(position, destination)
				&& board.isEmptySpot(destination);
	}

	private boolean pawnIsGoingNorth(Position p) {
		return board.get(p) instanceof Pawn && ((Pawn) board.get(p)).goingNorth();
	}

	private boolean pawnIsGoingSouth(Position p) {
		return board.get(p) instanceof Pawn && !((Pawn) board.get(p)).goingNorth();
	}

	private boolean destinationIsDiagonal(Position position, Position destination) {
		return Math.abs(destination.getX() - position.getX()) > 0;
	}

	private void castling(Position position, Position destination) {
		Piece piece = board.get(position);

		// Castling to the right.
		if (piece instanceof King && !piece.hasMoved() && destination.getX() == 6) {
			Position pos1 = new Position(7, destination.getY());
			Position pos2 = new Position(5, position.getY());
			moves.peek().setPosition3(pos1);
			moves.peek().setPosition4(pos2);
			board.move(pos1, pos2);
			board.toggleTurn();
		}

		// Castling to the left.
		if (piece instanceof King && !piece.hasMoved() && destination.getX() == 2) {
			Position pos1 = new Position(0, destination.getY());
			Position pos2 = new Position(3, position.getY());
			moves.peek().setPosition3(pos1);
			moves.peek().setPosition4(pos2);
			board.move(pos1, pos2);
			board.toggleTurn();
		}

	}

	private void pawnPromotion(Position position) {
		if (!(board.get(position) instanceof Pawn)) {
			return;
		}

		if (board.get(position).getTeam().equals(Team.WHITE) && isOnTopRow(position)) {
			Queen queen = new Queen(board, Team.WHITE);
			board.delete(position);
			board.add(queen, position);
		}

		if (board.get(position).getTeam().equals(Team.BLACK) && isOnBottomRow(position)) {
			Queen queen = new Queen(board, Team.BLACK);
			board.delete(position);
			board.add(queen, position);
		}

	}

	private void markEnPassantPosition(Position position, Position destination) {
		if (pawnMovedTwoSquares(position, destination)) {
			enPassant = new Position(position.getX(), (position.getY() + destination.getY()) / 2);
		} else {
			enPassant = null;
		}
	}

	private boolean pawnMovedTwoSquares(Position position, Position destination) {
		return board.get(position) instanceof Pawn && Math.abs(position.getY() - destination.getY()) > 1;
	}

	private void markAsMoved(Position position) {

		Piece piece = board.get(position);

		if (piece instanceof Rook) {
			piece.markAsMoved();
		} else if (piece instanceof King) {
			piece.markAsMoved();
		}
	}

	private boolean isOnTopRow(Position position) {
		return position.getY() == 0;
	}

	private boolean isOnBottomRow(Position position) {
		return position.getY() == 7;
	}

	void setEnPassant(Position enPassant) {
		this.enPassant = enPassant;
	}

	Position getEnPassant() {
		return enPassant;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		for (Move move : moves) {
			sb.append("\n");
			sb.append(move);
		}
		return new String(sb);
	}

}
