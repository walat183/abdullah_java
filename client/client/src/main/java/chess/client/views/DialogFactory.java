package chess.client.views;

import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.layout.StackPane;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.stage.StageStyle;

public class DialogFactory {

	@SuppressWarnings("static-access")
	public static void showDialogView(String title, String content) {
		System.out.println("fail !");
		Stage dialog = new Stage();
		dialog.initStyle(StageStyle.UTILITY);
		Text text = new Text(100, 25, content);
		StackPane root = new StackPane(text);
		root.setAlignment(text, Pos.BOTTOM_LEFT);
		Scene scene = new Scene(root);
		dialog.setTitle(title);
		dialog.setScene(scene);
		dialog.showAndWait();
	}

}
