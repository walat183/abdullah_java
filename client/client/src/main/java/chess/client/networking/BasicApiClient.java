package chess.client.networking;

import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import chess.client.entities.GameState;
import chess.client.entities.MoveRequest;
import chess.client.entities.MoveResponse;
import chess.client.entities.PlayerRegistration;

public class BasicApiClient implements ApiClient {

	private final RestTemplate restTemplate;
	private final String BASE_URL;

	public BasicApiClient(RestTemplate restTemplate, String baseUrl) {
		this.restTemplate = restTemplate;
		this.BASE_URL = baseUrl;
	}

	@Override
	public String getGameId() {
		RestTemplate restTemplate = new RestTemplate();
		return restTemplate.getForObject(BASE_URL + "/games", String.class);
	}

	@Override
	public PlayerRegistration getPlayerId(String gameId) {
		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<PlayerRegistration> playerIdResponse = restTemplate
				.postForEntity(BASE_URL + "/players/" + gameId, null, PlayerRegistration.class);
		if (playerIdResponse.getStatusCode().is2xxSuccessful()) {
			return playerIdResponse.getBody();
		}
		throw new RuntimeException("networking exception");
	}

	@Override
	public GameState getGameState(String gameId, String playerId) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public MoveResponse postMove(String gameId, MoveRequest moveRequest) {
		// TODO Auto-generated method stub
		return null;
	}


}
