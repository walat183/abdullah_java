package chess.client.viewmodel;

import chess.client.entities.PlayerRegistration;
import chess.client.model.DataModelManager;
import chess.client.tasks.AsyncClientCallsManager;
import chess.client.views.DialogFactory;
import chess.client.views.ViewLoader;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.concurrent.Task;

public class LobbyViewModel {

	private StringProperty newGameId;
	private StringProperty joinGameId;
	private AsyncClientCallsManager asyncClientCallsManager;

	public LobbyViewModel(AsyncClientCallsManager asyncClientCallsManager) {
		newGameId = new SimpleStringProperty("");
		joinGameId = new SimpleStringProperty("");
		this.asyncClientCallsManager = asyncClientCallsManager;
	}

	public StringProperty getNewGameIdProperty() {
		return newGameId;
	}

	public StringProperty getJoinGameIdProperty() {
		return joinGameId;
	}

	public void getNewGameId() {
		Task<String> newGameIdTask = asyncClientCallsManager.getNewGameIdRequestingTask();
		newGameIdTask.setOnSucceeded(event -> newGameId.set(newGameIdTask.getValue()));
		newGameIdTask.setOnFailed(event -> DialogFactory.showDialogView("Error", "Try again later!"));
		newGameIdTask.run();
	}

	public void joinGameById() {
		Task<PlayerRegistration> registerPlayerIdTask = asyncClientCallsManager
				.getRegisterPlayerIdTask(joinGameId.get());
		registerPlayerIdTask.setOnSucceeded(event -> viewChessGame(registerPlayerIdTask.getValue()));
		registerPlayerIdTask.setOnFailed(event -> {
			DialogFactory.showDialogView("Error", "Try again later!");
		});

		registerPlayerIdTask.run();
	}

	private void viewChessGame(PlayerRegistration playerRegistration) {

		System.out.println("success !!");
		String gameId = this.joinGameId.get();
		DataModelManager dataModelManager = new DataModelManager(gameId, playerRegistration.getPlayerId(),
				playerRegistration.getTeam());

	}

}
