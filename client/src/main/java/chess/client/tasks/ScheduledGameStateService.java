package chess.client.tasks;

import chess.client.entities.GameState;
import chess.client.networking.ApiClient;
import chess.client.networking.GameStateRequestConfig;
import javafx.concurrent.ScheduledService;
import javafx.concurrent.Task;

public class ScheduledGameStateService extends ScheduledService<GameState> {

	private final ApiClient apiClient;
	private final GameStateRequestConfig gameConfig;

	public ScheduledGameStateService(ApiClient apiClient, GameStateRequestConfig gameConfig) {
		this.apiClient = apiClient;
		this.gameConfig = gameConfig;
	}

	@Override
	protected Task<GameState> createTask() {
		Task<GameState> gameStateRequestingTask = new Task<GameState>() {

			@Override
			protected GameState call() throws Exception {
				return apiClient.getGameState(gameConfig.getGameId(), gameConfig.getPlayerId());
			}
		};
		return gameStateRequestingTask;
	}

}
