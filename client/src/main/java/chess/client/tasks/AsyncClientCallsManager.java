package chess.client.tasks;

import chess.client.entities.MoveRequest;
import chess.client.networking.ApiClient;
import chess.client.networking.GameStateRequestConfig;
import javafx.concurrent.Task;

public class AsyncClientCallsManager {

	private ApiClient apiClient;

	public AsyncClientCallsManager(ApiClient apiClient) {
		this.apiClient = apiClient;
	}

	public Task<String> getNewGameIdRequestingTask() {
		return new NewGameIdTask(apiClient);
	}

	public RegisterPlayerIdTask getRegisterPlayerIdTask(String gameId) {
		return new RegisterPlayerIdTask(apiClient, gameId);
	}

	public PostPlayerMoveTask getRegisterPlayerIdTask(String gameId, MoveRequest moveRequest) {
		return new PostPlayerMoveTask(apiClient, gameId, moveRequest);
	}

	public ScheduledGameStateService getGameStateScheduledService(String gameId, String playerId) {
		GameStateRequestConfig gameConfig = new GameStateRequestConfig(gameId, playerId);
		return new ScheduledGameStateService(apiClient, gameConfig);
	}

}
