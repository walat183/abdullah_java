package chess.client.tasks;

import chess.client.entities.MoveRequest;
import chess.client.entities.MoveResponse;
import chess.client.networking.ApiClient;
import javafx.concurrent.Task;

public class PostPlayerMoveTask extends Task<MoveResponse> {

	private ApiClient apiClient;
	private String gameId;
	private MoveRequest moveRequest;

	public PostPlayerMoveTask(ApiClient apiClient, String gameId, MoveRequest moveRequest) {
		this.apiClient = apiClient;
		this.gameId = gameId;
		this.moveRequest = moveRequest;
	}

	@Override
	protected MoveResponse call() throws Exception {
		return apiClient.postMove(gameId, moveRequest);
	}

}