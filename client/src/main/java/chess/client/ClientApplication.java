package chess.client;

import org.springframework.boot.autoconfigure.SpringBootApplication;

import chess.client.networking.ApiClient;
import chess.client.networking.BasicApiClient;
import chess.client.networking.RestTemplateFactory;
import chess.client.statics.ServicesConfig;
import chess.client.tasks.AsyncClientCallsManager;
import chess.client.views.ViewLoader;
import javafx.application.Application;
import javafx.stage.Stage;

@SpringBootApplication
public class ClientApplication extends Application {

	public static void main(String[] args) {
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws Exception {
		ViewLoader.setInstance(primaryStage);
		ApiClient apiClient = new BasicApiClient(RestTemplateFactory.getInstance(),
				ServicesConfig.BASE_URL);
		AsyncClientCallsManager asyncClientCallsManager = new AsyncClientCallsManager(apiClient);
		ViewLoader.getInstance().get().loadChessLobbyView(asyncClientCallsManager);
	}

}
