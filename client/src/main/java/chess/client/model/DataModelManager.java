package chess.client.model;

import chess.client.entities.GameState;
import chess.client.entities.Team;

public class DataModelManager {

	private GameState gameState;
	private final String gameId;
	private final String playerId;
	private final Team myTeam;

	public DataModelManager(String gameId, String playerId, Team myTeam) {
		this.gameId = gameId;
		this.playerId = playerId;
		this.myTeam = myTeam;
	}

	public GameState getGameState() {
		return gameState;
	}

	public void setGameState(GameState gameState) {
		this.gameState = gameState;
	}

	public String getGameId() {
		return gameId;
	}

	public String getPlayerId() {
		return playerId;
	}

	public Team getMyTeam() {
		return myTeam;
	}

	public boolean gameOver() {
		return this.gameState.isGameOver();
	}

	public String getWinner() {
		return this.gameState.getWinner().toString();
	}

}
