package chess.client.statics;

import javafx.util.Duration;

public class ServicesConfig {

	public static Duration SERVICE_PERIOD = Duration.seconds(1.5);
	public static final String BASE_URL = "http://localhost:8080";

}
