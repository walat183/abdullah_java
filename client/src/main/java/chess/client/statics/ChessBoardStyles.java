package chess.client.statics;

import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.paint.Color;

public class ChessBoardStyles {

	public static Color BOARD_UNIT_INITIAL_FILL_COLOR = Color.LIGHTGRAY;
	public static Color BOARD_UNIT_FOCUSED_FILL_COLOR = Color.DARKGRAY;

	public static Color BOARD_BORDER_COLOR = Color.BLACK;
	public static BorderWidths BORDER_WIDTH = BorderWidths.DEFAULT;
	public static BorderStrokeStyle BORDER_STYLE = BorderStrokeStyle.DOTTED;

}
