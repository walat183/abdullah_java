package chess.client.manager;

import chess.client.model.DataModelManager;
import chess.client.networking.ApiClient;

public class ChessClientManager {

	private ApiClient apiClient;
	private DataModelManager dataModelManager;

	public ChessClientManager(ApiClient apiClient, DataModelManager dataModelManager) {
		this.apiClient = apiClient;
		this.dataModelManager = dataModelManager;
	}

}
