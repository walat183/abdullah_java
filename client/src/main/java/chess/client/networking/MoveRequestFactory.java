package chess.client.networking;

import chess.client.entities.BoardUnit;
import chess.client.entities.MoveRequest;

public class MoveRequestFactory {

	public static MoveRequest getInstance(BoardUnit start, BoardUnit destination, String playerId) {
		MoveRequest moveRequest = new MoveRequest();
		moveRequest.setStartX(start.getX());
		moveRequest.setStartY(start.getY());
		moveRequest.setDestinationX(destination.getX());
		moveRequest.setDestinationY(destination.getY());
		moveRequest.setPlayerId(playerId);
		return moveRequest;
	}

}
