package chess.client.networking;

public class GameStateRequestConfig {

	private final String gameId;
	private final String playerId;

	public GameStateRequestConfig(String gameId, String playerId) {
		this.gameId = gameId;
		this.playerId = playerId;
	}

	public String getGameId() {
		return gameId;
	}

	public String getPlayerId() {
		return playerId;
	}
}
