package chess.client.controllers;

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.image.ImageView;

public class TestController {

	@FXML
	private Button origin;
	@FXML
	private Button shape;
	@FXML
	private Button color;
	@FXML
	private ImageView imageView;

	private TestViewMode vm;

	public void init(TestViewMode vm) {
		this.vm = vm;
		// this.origin.graphicProperty().bindBidirectional(vm.getPr());
		this.origin.backgroundProperty().bindBidirectional(vm.getBr());
		color.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				// viewModel.onBoardClick(GridPane.getColumnIndex(button),
				// GridPane.getRowIndex(button));
				System.out.println("hello world");
			}
		});

		shape.setOnAction(new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent event) {
				// viewModel.onBoardClick(GridPane.getColumnIndex(button),
				// GridPane.getRowIndex(button));
				System.out.println("hello world");
			}
		});
	}

}
