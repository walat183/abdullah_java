package chess.client.controllers;

import java.util.Random;

import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.paint.Paint;

public class TestViewMode {

	private Property<Node> icon;
	private Property<Background> pcolor;

	public TestViewMode() {
		System.out.println(getClass());
		String url = getClass().getResource("/ChessPiece/Black_Bishop.png").toString();
		Image im = new Image(url);
		icon = new SimpleObjectProperty<Node>(new ImageView(im));

		BackgroundFill bgf = new BackgroundFill(Paint.valueOf("red"), null, null);
		BackgroundFill[] fills = new BackgroundFill[1];
		fills[0] = bgf;
		Background bg = new Background(fills);
		pcolor = new SimpleObjectProperty<Background>(bg);

		new Thread(new Runnable() {

			@Override
			public void run() {
				while (true) {
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					String color = "red";
					int r = new Random().nextInt(3);
					switch (r) {
					case 0:
						color = "blue";
						break;
					case 1:
						color = "red";
						break;
					default:
						color = "green";
						break;
					}

					String u = "";
					r = new Random().nextInt(3);
					switch (r) {
					case 0:
						u = "/ChessPiece/White_Bishop.png";
						break;
					case 1:
						u = "/ChessPiece/Black_Rook.png";
						break;
					default:
						u = "/ChessPiece/Black_Queen.png";
						break;
					}

					BackgroundFill bgf = new BackgroundFill(Paint.valueOf(color), null, null);
					BackgroundFill[] fills = new BackgroundFill[1];
					fills[0] = bgf;
					Image im = new Image(u);
					BackgroundImage image = new BackgroundImage(im, null, null, null, null);
					BackgroundImage[] images = new BackgroundImage[1];
					images[0] = image;
					pcolor.setValue(new Background(fills, images));
					System.out.println(color);
					System.out.println(u);

				}

			}
		}).start();
	}

	public Property<Node> getPr() {
		return icon;
	}

	public Property<Background> getBr() {
		return pcolor;
	}

}
