package chess.client.controllers;

import chess.client.viewmodel.ChessGameViewModel;
import javafx.beans.property.Property;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.layout.GridPane;

public class ChessGameController {

	@FXML
	private Label playerTeam;

	@FXML
	private Label playerTurn;

	@FXML
	private GridPane chessBoard;

	private ChessGameViewModel viewModel;

	public void init(ChessGameViewModel viewModel) {
		this.viewModel = viewModel;
		this.playerTeam.textProperty().bindBidirectional(viewModel.getPlayerTeamProperty());
		this.playerTurn.textProperty().bindBidirectional(viewModel.getPlayerTurnProperty());

		for (Node node : chessBoard.getChildren()) {
			Button button = (Button) node;
			int x = GridPane.getColumnIndex(button);
			int y = GridPane.getRowIndex(button);
			Property<Background> backgroundproperty = this.viewModel.getBoardUnitPropertiesByCoordinates(x, y);
			Property<Border> borderProperty = this.viewModel.getBoardUnitBorderPropertiesByCoordinates(x, y);
			button.backgroundProperty().bindBidirectional(backgroundproperty);
			button.borderProperty().bindBidirectional(borderProperty);
			button.setOnAction(new EventHandler<ActionEvent>() {
				@Override
				public void handle(ActionEvent event) {
					int x = GridPane.getColumnIndex(button);
					int y = GridPane.getRowIndex(button);
					viewModel.onBoardClick(x, y);
				}
			});
		}

	}

	@FXML
	private void backToLobby(ActionEvent actionEvent) {
		this.viewModel.switchToLobbyView();
	}
}
