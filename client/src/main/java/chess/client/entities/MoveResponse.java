package chess.client.entities;

public class MoveResponse {

	private boolean isAccepted;
	private String message;

	public MoveResponse() {
	}

	public MoveResponse(boolean isAccepted, String message) {
		this.isAccepted = isAccepted;
		this.message = message;
	}

	public boolean isAccepted() {
		return isAccepted;
	}

	public void setAccepted(boolean isAccepted) {
		this.isAccepted = isAccepted;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}