package chess.client.viewmodel;

import java.util.Optional;

import chess.client.entities.BoardUnit;
import chess.client.entities.GameState;
import chess.client.entities.MoveRequest;
import chess.client.entities.MoveResponse;
import chess.client.model.ChessClientLogicManager;
import chess.client.model.DataModelManager;
import chess.client.networking.MoveRequestFactory;
import chess.client.statics.ChessBoardStyles;
import chess.client.statics.ServicesConfig;
import chess.client.tasks.AsyncClientCallsManager;
import chess.client.tasks.PostPlayerMoveTask;
import chess.client.tasks.ScheduledGameStateService;
import chess.client.views.DialogFactory;
import chess.client.views.ViewLoader;
import javafx.application.Platform;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;

public class ChessGameViewModel {

	private StringProperty playerTeam;
	private StringProperty playerTurn;
	private BoardUnitPropertiesManager boardUnitPropertiesManager;
	private DataModelManager dataModelManager;
	private AsyncClientCallsManager asyncClientCallsManager;
	private ChessClientLogicManager chessClientLogicManager;
	private boolean gameStateFetched;
	private ScheduledGameStateService scheduledGameStateService;

	public ChessGameViewModel(DataModelManager dataModelManager, AsyncClientCallsManager asyncClientCallsManager) {
		this.gameStateFetched = false;
		this.chessClientLogicManager = new ChessClientLogicManager();
		this.playerTeam = new SimpleStringProperty();
		this.playerTurn = new SimpleStringProperty();
		this.dataModelManager = dataModelManager;
		this.boardUnitPropertiesManager = new BoardUnitPropertiesManager(
				ChessBoardStyles.BOARD_UNIT_INITIAL_FILL_COLOR);
		this.asyncClientCallsManager = asyncClientCallsManager;
		initializeGameStateFetchingService();
	}

	private void initializeGameStateFetchingService() {
		this.scheduledGameStateService = this.asyncClientCallsManager
				.getGameStateScheduledService(dataModelManager.getGameId(), dataModelManager.getPlayerId());
		scheduledGameStateService.setOnSucceeded(event -> {
			Optional<GameState> gameSate = Optional.ofNullable(scheduledGameStateService.getLastValue());
			if (gameSate.isPresent()) {
				this.dataModelManager.setGameState(gameSate.get());
				Platform.runLater(new Runnable() {

					@Override
					public void run() {
						updateBoardProperties();
						updateGameLabels();
						endGameIfIsFinished();
					}
				});
			}
		});
		this.scheduledGameStateService.setOnFailed(event -> {
			DialogFactory.showDialogView("Error", "Try again later!");
		});
		this.scheduledGameStateService.setPeriod(ServicesConfig.SERVICE_PERIOD);
		this.scheduledGameStateService.start();

	}

	public Property<Background> getBoardUnitPropertiesByCoordinates(int columnIndex, int rowIndex) {
		return this.boardUnitPropertiesManager.getBoardUnitBackgroundPropertiesByCoordinates(columnIndex, rowIndex);
	}

	public Property<Border> getBoardUnitBorderPropertiesByCoordinates(int columnIndex, int rowIndex) {
		return this.boardUnitPropertiesManager.getBoardUnitBorderPropertiesByCoordinates(columnIndex, rowIndex);
	}

	private void updateBoardProperties() {
		BoardUnitPropertiesMapper.mapChessBoardUnitsProperties(this.boardUnitPropertiesManager, dataModelManager);
	}

	private void updateGameLabels() {
		if (!this.gameStateFetched) {
			BoardUnitPropertiesMapper.mapPlayerTeamProperty(playerTeam, dataModelManager);
			this.gameStateFetched = true;
		}
		BoardUnitPropertiesMapper.mapPlayerTurnProperty(playerTurn, dataModelManager);

	}

	public void onBoardClick(int x, int y) {
		boolean moveStarted = postMoveIfStartDestinationAvailable(x, y);
		this.boardUnitPropertiesManager.updateBoardUnitsFocusedProperties(x, y, moveStarted);
	}

	private boolean postMoveIfStartDestinationAvailable(int x, int y) {
		boolean startMoveTask = this.chessClientLogicManager.processClick(x, y);
		if (startMoveTask) {
			BoardUnit source = this.chessClientLogicManager.getSource();
			BoardUnit destination = this.chessClientLogicManager.getDestination();
			this.chessClientLogicManager.reset();
			MoveRequest moveRequest = MoveRequestFactory.getInstance(source, destination,
					dataModelManager.getPlayerId());
			startMoveRequestTask(moveRequest);
		}
		return startMoveTask;
	}

	private void startMoveRequestTask(MoveRequest moveRequest) {
		PostPlayerMoveTask postplayerMoveTask = this.asyncClientCallsManager
				.getRegisterPlayerIdTask(dataModelManager.getGameId(), moveRequest);
		postplayerMoveTask.setOnSucceeded(event -> {
			MoveResponse moveResponse = postplayerMoveTask.getValue();

			Platform.runLater(new Runnable() {
				@Override
				public void run() {
					if (moveResponse.isAccepted()) {
						int x = moveRequest.getStartX();
						int y = moveRequest.getStartY();
						int endX = moveRequest.getDestinationX();
						int endY = moveRequest.getDestinationY();
						boardUnitPropertiesManager.movePropertiesFromSourceToDestination(x, y, endX, endY);
					} else {
						DialogFactory.showDialogView("Move Not Allowed", moveResponse.getMessage());
					}
					scheduledGameStateService.reset();
					scheduledGameStateService.start();
				}
			});
		});
		postplayerMoveTask.setOnFailed(event -> {
			scheduledGameStateService.reset();
			scheduledGameStateService.start();
			DialogFactory.showDialogView("Move Not Allowed", "Try again later!");
		});

		this.scheduledGameStateService.cancel();
		postplayerMoveTask.run();
	}

	public Property<String> getPlayerTeamProperty() {
		return this.playerTeam;
	}

	public Property<String> getPlayerTurnProperty() {
		return this.playerTurn;
	}

	private void endGameIfIsFinished() {
		if (this.dataModelManager.gameOver()) {
			this.scheduledGameStateService.cancel();
			DialogFactory.showDialogView("Game finished!",
					"The " + this.dataModelManager.getWinner() + "has won the game!");
			ViewLoader.getInstance().get().loadChessLobbyView(asyncClientCallsManager);
		}
	}

	public void switchToLobbyView() {
		this.scheduledGameStateService.cancel();
		ViewLoader.getInstance().get().loadChessLobbyView(asyncClientCallsManager);
	}
}
