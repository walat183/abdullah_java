package chess.client.viewmodel;

import chess.client.statics.ChessBoardStyles;
import javafx.beans.property.Property;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.Image;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.BackgroundImage;
import javafx.scene.layout.BackgroundPosition;
import javafx.scene.layout.BackgroundRepeat;
import javafx.scene.layout.BackgroundSize;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.paint.Color;

public class BoardUnitProperties {

	private int x;
	private int y;
	private Property<Background> boardUnitProperty;
	private Property<Border> boardUnitBorderProperty;
	private boolean isFocused;
	private BackgroundImage[] images;
	private BackgroundFill[] fills;

	public BoardUnitProperties(int x, int y, Color unitBackgroundColor) {
		this.x = x;
		this.y = y;
		this.isFocused = false;
		this.fills = constructBackgroundFill(unitBackgroundColor);
		Background background = new Background(this.fills);
		this.boardUnitProperty = new SimpleObjectProperty<Background>(background);
		this.boardUnitBorderProperty = new SimpleObjectProperty<Border>();
		this.boardUnitBorderProperty = new SimpleObjectProperty<Border>(
				new Border(new BorderStroke(ChessBoardStyles.BOARD_BORDER_COLOR, ChessBoardStyles.BORDER_STYLE, null,
						ChessBoardStyles.BORDER_WIDTH)));
	}

	public Property<Background> getBoardUnitProperty() {
		return this.boardUnitProperty;
	}

	public Property<Border> getBoardUnitBorderProperty() {
		return this.boardUnitBorderProperty;
	}

	public void updateBoardUnitFillProperty(Color unitBackgroundColor) {
		this.fills = constructBackgroundFill(unitBackgroundColor);
		boardUnitProperty.setValue(new Background(this.fills, this.images));
	}

	public void updateBoardUnitIconProperty(String iconResourcePath) {
		this.images = constructBackgroundImage(iconResourcePath);
		if (this.isFocused) {
			this.fills = constructBackgroundFill(ChessBoardStyles.BOARD_UNIT_FOCUSED_FILL_COLOR);
		}
		boardUnitProperty.setValue(new Background(this.fills, this.images));
	}

	public static BackgroundImage[] constructBackgroundImage(String iconResourcePath) {
		Image image = new Image(iconResourcePath);

		BackgroundImage backgroundImage = new BackgroundImage(image, BackgroundRepeat.NO_REPEAT,
				BackgroundRepeat.NO_REPEAT, BackgroundPosition.CENTER,
				new BackgroundSize(30, 30, false, false, false, false));
		BackgroundImage[] BackgroundImages = new BackgroundImage[1];
		BackgroundImages[0] = backgroundImage;
		return BackgroundImages;
	}

	public static BackgroundFill[] constructBackgroundFill(Color unitBackgroundColor) {
		BackgroundFill backGroundFill = new BackgroundFill(unitBackgroundColor, null, null);
		BackgroundFill[] backGroundFills = new BackgroundFill[1];
		backGroundFills[0] = backGroundFill;
		return backGroundFills;
	}

	public void updateBackgroundToUnfocused() {
		this.fills = constructBackgroundFill(ChessBoardStyles.BOARD_UNIT_INITIAL_FILL_COLOR);
		boardUnitProperty.setValue(new Background(this.fills, this.images));
	}

	public void updateBackgroundToFocused() {
		this.fills = constructBackgroundFill(ChessBoardStyles.BOARD_UNIT_FOCUSED_FILL_COLOR);
		boardUnitProperty.setValue(new Background(this.fills, this.images));
	}

	public int getBoardUnitXPosition() {
		return this.x;
	}

	public int getBoardUnitYPosition() {
		return this.x;
	}

	public boolean hasCoordinates(int x, int y) {
		return this.x == x && this.y == y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BoardUnitProperties other = (BoardUnitProperties) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}

	public void setFocused() {
		this.isFocused = true;
	}

	public boolean isFocused() {
		return this.isFocused;
	}

	public void setUnfocused() {
		this.isFocused = false;
	}

	public void removeIconAndSetUnfocusedBackground() {
		this.fills = constructBackgroundFill(ChessBoardStyles.BOARD_UNIT_INITIAL_FILL_COLOR);
		this.images = new BackgroundImage[1];
		boardUnitProperty.setValue(new Background(this.fills, this.images));

	}

	public void cloneBoardUnitProperty(Property<Background> boardUnitProperty) {
		this.boardUnitProperty.setValue(boardUnitProperty.getValue());
	}

}
