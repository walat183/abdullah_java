package chess.client.viewmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javafx.beans.property.Property;
import javafx.scene.layout.Background;
import javafx.scene.layout.Border;
import javafx.scene.paint.Color;

public class BoardUnitPropertiesManager {

	private List<BoardUnitProperties> boardUnitsProperties;

	public BoardUnitPropertiesManager(Color unitBackgroundColor) {
		this.boardUnitsProperties = new ArrayList<>();
		for (int i = 0; i < 8; i++) {
			for (int j = 0; j < 8; j++) {
				BoardUnitProperties BoardUnitProperties = new BoardUnitProperties(i, j, unitBackgroundColor);
				this.boardUnitsProperties.add(BoardUnitProperties);
				System.out.println(i + " - " + j);
			}
		}
		System.out.println("size : " + boardUnitsProperties.size());
	}

	public Property<Background> getBoardUnitBackgroundPropertiesByCoordinates(int x, int y) {
		BoardUnitProperties boardUnitProperties = this.boardUnitsProperties.stream()
				.filter(bup -> bup.hasCoordinates(x, y)).findAny().orElseThrow(() -> new RuntimeException(""));
		return boardUnitProperties.getBoardUnitProperty();
	}

	public Property<Border> getBoardUnitBorderPropertiesByCoordinates(int x, int y) {
		BoardUnitProperties boardUnitProperties = this.boardUnitsProperties.stream()
				.filter(bup -> bup.hasCoordinates(x, y)).findAny().orElseThrow(() -> new RuntimeException(""));
		return boardUnitProperties.getBoardUnitBorderProperty();
	}

	public BoardUnitProperties getBoardUnitPropertiesByCoordinates(int x, int y) {
		BoardUnitProperties boardUnitProperties = this.boardUnitsProperties.stream()
				.filter(bup -> bup.hasCoordinates(x, y)).findAny().orElseThrow(() -> new RuntimeException(""));
		return boardUnitProperties;
	}

	public void updateBoardUnitsFocusedProperties(int x, int y, boolean moveStarted) {
		Optional<BoardUnitProperties> lastFocusedBoardUnit = getBoardUnitPropertiesByFocused();
		if (lastFocusedBoardUnit.isPresent()) {
			lastFocusedBoardUnit.get().setUnfocused();
			lastFocusedBoardUnit.get().updateBackgroundToUnfocused();
		}
		BoardUnitProperties boardUnit = getBoardUnitPropertiesByCoordinates(x, y);
		if (!moveStarted) {
			boardUnit.setFocused();
			boardUnit.updateBackgroundToFocused();
		}
	}

	public Optional<BoardUnitProperties> getBoardUnitPropertiesByFocused() {
		return this.boardUnitsProperties.stream().filter(bup -> bup.isFocused()).findAny();
	}

	public void movePropertiesFromSourceToDestination(int startX, int startY, int endX, int endY) {
		BoardUnitProperties sourceBoardUnit = getBoardUnitPropertiesByCoordinates(startX, startY);
		BoardUnitProperties destinationBoardUnit = getBoardUnitPropertiesByCoordinates(endX, endY);
		destinationBoardUnit.cloneBoardUnitProperty(sourceBoardUnit.getBoardUnitProperty());
		sourceBoardUnit.removeIconAndSetUnfocusedBackground();

	}

	public List<BoardUnitProperties> getBoardUnitsProperties() {
		return this.boardUnitsProperties;
	}

}
