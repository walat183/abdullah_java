package chess.client.viewmodel;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import chess.client.entities.BoardUnit;
import chess.client.entities.Team;
import chess.client.model.DataModelManager;
import chess.client.statics.ChessIconsRecourcePaths;
import javafx.beans.property.StringProperty;

public class BoardUnitPropertiesMapper {

	public static void mapPlayerTurnProperty(StringProperty playerTurnProperty, DataModelManager dataModelManager) {
		playerTurnProperty.set("Player Turn: " + dataModelManager.getGameState().getPlayerTurn().toString());
	}

	public static void mapPlayerTeamProperty(StringProperty playerTurnProperty, DataModelManager dataModelManager) {
		playerTurnProperty.set("Player Team: " + dataModelManager.getMyTeam().toString());
	}

	public static void mapChessBoardUnitsProperties(BoardUnitPropertiesManager boardUnitPropertiesManager,
			DataModelManager dataModelManager) {

		List<BoardUnit> whiteBoardUnits = dataModelManager.getGameState().getWhitePieces();
		List<BoardUnit> blackBoardUnits = dataModelManager.getGameState().getBlackPieces();
		List<BoardUnitProperties> updatedBoardUnitProperties = new ArrayList<>();
		System.out.println("---------------------------------");
		for (BoardUnit boardUnit : whiteBoardUnits) {
			System.out.println(boardUnit.getX() + " - " + boardUnit.getY());
			System.out.println(boardUnit.getPieceType());

			int x = boardUnit.getX();
			int y = boardUnit.getY();
			BoardUnitProperties boardUnitProperties = boardUnitPropertiesManager.getBoardUnitPropertiesByCoordinates(x,
					y);
			updatedBoardUnitProperties.add(boardUnitProperties);
			mapBoardUnit(boardUnitProperties, boardUnit, Team.WHITE);
		}
		for (BoardUnit boardUnit : blackBoardUnits) {
			System.out.println(boardUnit.getX() + " - " + boardUnit.getY());
			System.out.println(boardUnit.getPieceType());
			int x = boardUnit.getX();
			int y = boardUnit.getY();
			BoardUnitProperties boardUnitProperties = boardUnitPropertiesManager.getBoardUnitPropertiesByCoordinates(x,
					y);
			updatedBoardUnitProperties.add(boardUnitProperties);
			mapBoardUnit(boardUnitProperties, boardUnit, Team.BLACK);

		}
		System.out.println("---------------------------------");
		resetRemainingBoardUnitsPropertiesToDefault(boardUnitPropertiesManager, updatedBoardUnitProperties);

	}

	private static void resetRemainingBoardUnitsPropertiesToDefault(
			BoardUnitPropertiesManager boardUnitPropertiesManager,
			List<BoardUnitProperties> updatedBoardUnitProperties) {

		List<BoardUnitProperties> boardUnitsProperties = boardUnitPropertiesManager.getBoardUnitsProperties();
		List<BoardUnitProperties> remainingBoardUnitsProperties = boardUnitsProperties.stream()
				.filter(unit -> !updatedBoardUnitProperties.contains(unit)).collect(Collectors.toList());
		for (BoardUnitProperties boardUnitProperties : remainingBoardUnitsProperties) {
			boardUnitProperties.removeIconAndSetUnfocusedBackground();
		}

	}

	public static void mapBoardUnit(BoardUnitProperties boardUnitProperties, BoardUnit boardUnit, Team pieceTeam) {
		String chessIconResourcePath = getImageRecourcePath(boardUnit.getPieceType(), pieceTeam);
		boardUnitProperties.updateBoardUnitIconProperty(chessIconResourcePath);
	}

	public static String getImageRecourcePath(String type, Team pieceType) {

		String pieceIconPath = "";
		if (pieceType.equals(Team.BLACK)) {
			pieceIconPath = getBlackPieceIconPathByType(type);
		} else {
			pieceIconPath = getWhitePieceIconPathByType(type);
		}
		BoardUnitPropertiesMapper boardUnitPropertiesMapper = new BoardUnitPropertiesMapper();
		return boardUnitPropertiesMapper.getFullChessIconRescourcePath(pieceIconPath);
	}

	private String getFullChessIconRescourcePath(String iconPath) {
		return getClass().getResource(iconPath).toString();
	}

	private static String getWhitePieceIconPathByType(String type) {
		if (type.equals("Rook")) {
			return ChessIconsRecourcePaths.WHITE_ROOK_ICON_PATH;
		} else if (type.equals("Knight")) {
			return ChessIconsRecourcePaths.WHITE_KNIGHT_ICON_PATH;
		} else if (type.equals("Bishop")) {
			return ChessIconsRecourcePaths.WHITE_BISHOP_ICON_PATH;
		} else if (type.equals("Queen")) {
			return ChessIconsRecourcePaths.WHITE_QUEEN_ICON_PATH;
		} else if (type.equals("King")) {
			return ChessIconsRecourcePaths.WHITE_KING_ICON_PATH;
		} else if (type.equals("Pawn")) {
			return ChessIconsRecourcePaths.WHITE_PAWN_ICON_PATH;
		} else {
			throw new RuntimeException("Undefied Type!");
		}
	}

	private static String getBlackPieceIconPathByType(String type) {
		if (type.equals("Rook")) {
			return ChessIconsRecourcePaths.BLACK_ROOK_ICON_PATH;
		} else if (type.equals("Knight")) {
			return ChessIconsRecourcePaths.BLACK_KNIGHT_ICON_PATH;
		} else if (type.equals("Bishop")) {
			return ChessIconsRecourcePaths.BLACK_BISHOP_ICON_PATH;

		} else if (type.equals("Queen")) {
			return ChessIconsRecourcePaths.BLACK_QUEEN_ICON_PATH;

		} else if (type.equals("King")) {
			return ChessIconsRecourcePaths.BLACK_KING_ICON_PATH;

		} else if (type.equals("Pawn")) {
			return ChessIconsRecourcePaths.BLACK_PAWN_ICON_PATH;

		} else {
			throw new RuntimeException("Undefied Type!");
		}
	}

}
